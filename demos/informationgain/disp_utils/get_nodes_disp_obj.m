function display_obj = get_nodes_disp_obj(nodes, value, map,display_obj,first)
%GET_INFORMATION_DISP_OBJ Summary of this function goes here
%   Detailed explanation goes here
if (nargin<3)
    display_obj = struct();
    display('Error get_nodes_disp_obj needs a display_obj with maps.')
    return;
end
if(~first)
    delete(display_obj.handle_set.robo_view_nodes);
    delete(display_obj.handle_set.world_view_nodes);
end

% Figure 1: Robo display
set(0,'CurrentFigure',display_obj.handle_set.fig_robo_view);
hold on;

nodes = coord2indexNotRound(nodes(:,1),nodes(:,2),map.scale,map.min(1),map.min(2));
 
display_obj.handle_set.robo_view_nodes = plot(nodes(:,1),nodes(:,2),'o');
axis xy;
axis equal;

% Figure 2: WorldMap display
set(0,'CurrentFigure',display_obj.handle_set.fig_world);
hold on;
display_obj.handle_set.world_view_nodes = plot(nodes(:,1),nodes(:,2),'o');
axis xy;
axis equal;
end

