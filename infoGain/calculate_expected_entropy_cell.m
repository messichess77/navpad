function [ expected_entropy, probability_ray_passes ] = calculate_expected_entropy_cell( grid_log_odds,grid_params )
%CALCULATE_INFO_GAIN_CELL Summary of this function goes here
%   Detailed explanation goes here
[p_occ p_empty] = logodds2prob(grid_log_odds);

occ_log_odds = grid_log_odds + grid_params.log_odds_occ_sensor;
free_log_odds = grid_log_odds + grid_params.log_odds_empty_sensor;

[p_observed_occ ~] = logodds2prob(occ_log_odds);
[p_observed_free ~] = logodds2prob(free_log_odds);

entropy_observed_occ = calculate_entropy_bernoulli(p_observed_occ);
entropy_observed_free = calculate_entropy_bernoulli(p_observed_free);

expected_entropy = (p_occ*grid_params.p_occ_sensor + p_empty*(1-grid_params.p_empty_sensor)).*entropy_observed_occ...
                + (p_occ*(1-grid_params.p_occ_sensor) + p_empty*grid_params.p_empty_sensor).*entropy_observed_free;
            
probability_ray_passes = p_empty*grid_params.p_empty_sensor + p_occ*(1-grid_params.p_empty_sensor);
                   
end