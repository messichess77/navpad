function [globalPathList,globalPathIds,stateMatrix] = generateAllPaths(minState,maxState,resolutionState,rollResolution,rollMaxAngle,rollRate,decceleration,maxDeccZ,deccZResolution,vMin,reactionTime)
                                    
%GENERATEALLPATHS Summary of this function goes here
%   Detailed explanation goes here
X = [];
Y = [];
Z = [];
Roll = [];
Heading = [];
T = [];
[vxMatrix,~,~] = ndgrid(minState(1):resolutionState(1):maxState(1),minState(2):resolutionState(2):maxState(2),minState(3):resolutionState(3):maxState(3));
sizeVMatrix = size(vxMatrix);
metaSize = size(size(vxMatrix));
if (metaSize(2)~=3)
    sizeVMatrix(3)=1;
end

stateMatrix(sizeVMatrix(1),sizeVMatrix(2),sizeVMatrix(3)).pathIds = [];
stateMatrix(sizeVMatrix(1),sizeVMatrix(2),sizeVMatrix(3)).vx = [];
stateMatrix(sizeVMatrix(1),sizeVMatrix(2),sizeVMatrix(3)).vz = [];
stateMatrix(sizeVMatrix(1),sizeVMatrix(2),sizeVMatrix(3)).roll = [];
stateMatrix(sizeVMatrix(1),sizeVMatrix(2),sizeVMatrix(3)).boundingBox = [];
globalPathList = [];


i=0;
j=0;
k=0;

for vX = minState(1):resolutionState(1):maxState(1)
    i =i+1;
    for vZ = minState(2):resolutionState(2):maxState(2)
        j =j+1;
        for roll = minState(3):resolutionState(3):maxState(3)
            tic
            k = k+1;
            [ x,y,z,h,r,t ] = generateAllPathsForState(vX,vZ,roll,rollResolution,rollMaxAngle,rollRate,decceleration,maxDeccZ,deccZResolution,vMin,reactionTime,10000);
            globalPathList = [globalPathList;t,x,y,z,h,r,vX*ones(size(x)),vZ*ones(size(x))];
            stateMatrix(i,j,k).vx = vX;
            stateMatrix(i,j,k).vz = vZ;
            stateMatrix(i,j,k).roll = roll;
            toc
        end
        k=0;
    end
    j=0;
end

globalPathIds = find(globalPathList(:,1)==0);
globalPathIds = [globalPathIds,zeros(size(globalPathIds))];
for i=1:size(globalPathIds,1)-1
    globalPathIds(i,2) = globalPathIds(i+1,1)-1;
end

globalPathIds(end,2) = size(globalPathList,1);

for i=1:size(globalPathIds,1)
    vx = globalPathList(globalPathIds(i),7);
    vz = globalPathList(globalPathIds(i),8);
    roll = globalPathList(globalPathIds(i),6);
    vId = value2Id([vx,vz,roll],minState,resolutionState);
    stateMatrix(vId(1),vId(2),vId(3)).pathIds(end+1,1) = i;
end
i=0;j=0;k=0;
for vX = minState(1):resolutionState(1):maxState(1)
    i =i+1;
    for vZ = minState(2):resolutionState(2):maxState(2)
        j =j+1;
        for roll = minState(3):resolutionState(3):maxState(3)
            k = k+1;
            minBBX = [];
            maxBBX = [];
            minBBY = [];
            maxBBY = [];
            for l=1:size(stateMatrix(i,j,k).pathIds,1)
                idx = stateMatrix(i,j,k).pathIds(l);
                minBBY = min([globalPathList(globalPathIds(idx,1):globalPathIds(idx,2),3);minBBY]);
                maxBBY = max([globalPathList(globalPathIds(idx,1):globalPathIds(idx,2),3);maxBBY]);
                
                minBBX = min([globalPathList(globalPathIds(idx,1):globalPathIds(idx,2),2);minBBX]);
                maxBBX = max([globalPathList(globalPathIds(idx,1):globalPathIds(idx,2),2);maxBBX]);
            end
            stateMatrix(i,j,k).boundingBox = [minBBX,minBBY;maxBBX,maxBBY];
        end
        k=0;
    end
    j=0;
end
